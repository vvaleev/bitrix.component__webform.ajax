<?

	if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	{
		die();
	}

	use Bitrix\Main\Localization\Loc;

	$forms = [
		0 => Loc::getMessage('WEB_FORM_AJAX_PARAMETER_NOT_VALUE')
	];

	if(CModule::IncludeModule('form'))
	{
		$rsForm = \CForm::GetList($by = 's_sort', $order = 'asc', !empty($_REQUEST['site']) ? ['SITE' => $_REQUEST['site']] : [], $isFiltered);

		while($arForm = $rsForm->Fetch())
		{
			$forms[$arForm['ID']] = "[{$arForm['ID']}] {$arForm['NAME']}";
		}
	}

	$eventMailTypes   = [];
	$rsEventMailTypes = \CEventType::GetList(['LID' => LANGUAGE_ID]);

	while($eventMailType = $rsEventMailTypes->Fetch())
	{
		$eventMailTypes[$eventMailType['EVENT_NAME']] = $eventMailType['NAME'];
	}

	$mailTemplates   = [];
	$rsMailTemplates = \CEventMessage::GetList($by = 'site_id', $order = 'desc', [
		'TYPE_ID' => [$arCurrentValues['EVENT_MAIL_TYPE_ID']]
	]);

	while($mailTemplate = $rsMailTemplates->Fetch())
	{
		$mailTemplates[$mailTemplate['ID']] = $mailTemplate['SUBJECT'];
	}

	$arComponentParameters = [
		'PARAMETERS' => [
			'SHOW_ERRORS'            => [
				'NAME'    => Loc::getMessage('WEB_FORM_AJAX_PARAMETER_SHOW_ERRORS'),
				'TYPE'    => 'CHECKBOX',
				'DEFAULT' => 'N',
			],
			'WEB_FORM_ID'            => [
				'PARENT' => 'DATA_SOURCE',
				'NAME'   => Loc::getMessage('WEB_FORM_AJAX_PARAMETER_WEB_FORM_ID'),
				'TYPE'   => 'LIST',
				'VALUES' => $forms,
			],
			'EVENT_MAIL_TYPE_ID'     => [
				'PARENT'  => 'BASE',
				'NAME'    => Loc::getMessage('WEB_FORM_AJAX_PARAMETER_EVENT_MAIL_TYPE_ID'),
				'TYPE'    => 'LIST',
				'VALUES'  => $eventMailTypes,
				'REFRESH' => 'Y',
			],
			'EVENT_MAIL_TEMPLATE_ID' => [
				'PARENT' => 'BASE',
				'NAME'   => Loc::getMessage('WEB_FORM_AJAX_PARAMETER_EVENT_MAIL_TEMPLATE_ID'),
				'TYPE'   => 'LIST',
				'VALUES' => $mailTemplates,
			],
		]
	];
