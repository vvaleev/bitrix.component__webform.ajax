<?

	if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	{
		die();
	}

	use Bitrix\Main\Context;
	use Bitrix\Main\Engine\Contract\Controllerable;
	use Bitrix\Main\Web\Json;

	class WebFormAjaxComponent extends \CBitrixComponent implements Controllerable
	{
		/**
		 * Метод для подготовки параметров компонента.
		 *
		 * @param array $arParams - массив параметров.
		 *
		 * @return array
		 */
		public function onPrepareComponentParams($arParams)
		{
			return $arParams;
		}

		/**
		 * Метод для инициализации компонента.
		 *
		 * @return mixed|void
		 * @throws \Bitrix\Main\ArgumentException
		 */
		public function executeComponent()
		{
			if(CModule::IncludeModule('form'))
			{
				global $APPLICATION;

				$postData = Context::getCurrent()
								   ->getRequest()
								   ->getPostList()
								   ->toArray()
				;

				if($postData['FORM_DATA'] === 'Y')
				{
					$APPLICATION->RestartBuffer();
					die(Json::encode($this->formDataAction($postData)));
				}

				$formData       = $this->getFormDataByID($this->arParams['WEB_FORM_ID']);
				$formParameters = $this->getFormParametersByID($this->arParams['WEB_FORM_ID']);

				$this->arResult['FORM_DATA']['NAME']               = $formData['form']['NAME'];
				$this->arResult['FORM_DATA']['SUBMIT_BUTTON_NAME'] = $formData['form']['BUTTON'];
				$this->arResult['FORM_DATA']['QUESTIONS_COUNT']    = $formData['form']['QUESTIONS'];
				$this->arResult['FORM_DATA']['USE_CAPTCHA']        = $formParameters['USE_CAPTCHA'];
				$this->arResult['FORM_DATA']['ITEMS']              = $this->getFormElements($formData['questions'], $formData['answers']);

				if($this->arResult['FORM_DATA']['USE_CAPTCHA'] === 'Y')
				{
					$this->arResult['FORM_DATA']['CAPTCHA'] = $this->generateCaptcha();
				}

				unset($formData);
				unset($formParameters);
			}

			$this->includeComponentTemplate();
		}

		/**
		 * Метод для работы с данными клиентской формы.
		 *
		 * @param array $formData Данные формы.
		 *
		 * @return array|bool
		 */
		public function formDataAction($formData)
		{
			$resultData = [];

			if(empty($formData) || !is_array($formData))
			{
				return false;
			}

			if($formData['CAPTCHA_KEY'] || $formData['CAPTCHA_SID'])
			{
				if(!$this->checkCaptcha($formData['CAPTCHA_KEY'], $formData['CAPTCHA_SID']))
				{
					return [
						'response' => [
							'error' => [
								'error_code' => 'captcha_error'
							]
						]
					];
				}
			}

			$checkErrorListItems = $this->checkFormData($formData);

			if(!empty($checkErrorListItems))
			{
				return [
					'response' => [
						'error' => [
							'required_items' => $checkErrorListItems
						]
					]
				];
			}

			if($formDataResult = $this->addFormData($this->arParams['WEB_FORM_ID'], $this->prepareFormData($formData)))
			{
				$this->sendMessageToUsers($this->arParams['EVENT_MAIL_TYPE_ID'], $this->arParams['EVENT_MAIL_TEMPLATE_ID'], $this->getEmails($formData), $this->getFiles($formDataResult));

				$resultData = [
					'success' => [
						'status_code' => 'Y'
					]
				];
			}

			return [
				'response' => $resultData
			];
		}

		/**
		 * Метод для проверки captcha.
		 *
		 * @param string|int $captchaKey Ключ captcha.
		 * @param string|int $captchaSid Идентификатор captcha.
		 *
		 * @return bool
		 */
		private function checkCaptcha($captchaKey, $captchaSid)
		{
			global $APPLICATION;

			if($APPLICATION->CaptchaCheckCode($captchaKey, $captchaSid))
			{
				return true;
			}

			return false;
		}

		/**
		 * Метод для проверки данных формы.
		 *
		 * @param array $formData Данные формы.
		 *
		 * @return array
		 */
		private function checkFormData($formData)
		{
			$data     = $this->getFormDataByID($this->arParams['WEB_FORM_ID']);
			$items    = $this->getFormElements($data['questions'], $data['answers']);
			$arResult = [];

			unset($data);

			foreach($items as $question)
			{
				$name = str_replace('[]', '', $question['NAME']);

				if($question['REQUIRED'] === 'Y' && empty($formData[$name]))
				{
					$arResult[$name] = $question['TITLE'];

					if(($question['TYPE'] === 'file' || $question['TYPE'] === 'image') && $_FILES[$name] && $_FILES[$name]['size'] > 0)
					{
						unset($arResult[$name]);
					}
				}
			}

			unset($items);

			return $arResult;
		}

		/**
		 * Метод для получения данных с формы по её идентификатору.
		 *
		 * @param {integer|string} $formId Идентификатор формы.
		 *
		 * @return array|bool
		 */
		private function getFormDataByID($formId)
		{
			if(!$formId)
			{
				return false;
			}

			\CForm::GetDataByID($formId, $form, $questions, $answers, $dropDown, $multiSelect);

			return [
				'form'        => $form,
				'questions'   => $questions,
				'answers'     => $answers,
				'dropDown'    => $dropDown,
				'multiSelect' => $multiSelect,
			];
		}

		/**
		 * Метод для получения элементов формы.
		 *
		 * @param array $questions Массив вопросов формы.
		 * @param array $answers   Массив ответов формы.
		 *
		 * @return array|bool
		 */
		private function getFormElements($questions, $answers)
		{
			if(!$questions || !$answers)
			{
				return false;
			}

			$elements = [];

			foreach($answers as $sid => $arAnswer)
			{
				$elements[$sid] = [
					'ACTIVE'     => $questions[$sid]['ACTIVE'],
					'ID'         => $questions[$sid]['ID'],
					'IMAGE_SIZE' => null,
					'IMAGE_SRC'  => (!empty($questions[$sid]['IMAGE_ID'])) ? \CFile::GetFileArray($questions[$sid]['IMAGE_ID'])['SRC'] : null,
					'REQUIRED'   => $questions[$sid]['REQUIRED'],
					'SID'        => $sid,
					'SORT'       => $questions[$sid]['C_SORT'],
					'TITLE'      => $questions[$sid]['TITLE'],
					'TYPE'       => $arAnswer[0]['FIELD_TYPE'],
					'NAME'       => $this->getElementName([
						'ID'   => $arAnswer[0]['ID'],
						'SID'  => $sid,
						'TYPE' => $arAnswer[0]['FIELD_TYPE'],
					]),
				];

				if(substr($elements[$sid]['IMAGE_SRC'], 0, 1) === '/')
				{
					$arSize = \CFile::GetImageSize($_SERVER['DOCUMENT_ROOT'] . $elements[$sid]['IMAGE_SRC']);

					if(is_array($arSize))
					{
						$elements[$sid]['IMAGE_SIZE'] = $arSize;
					}
				}

				foreach($arAnswer as $key => $answer)
				{
					$elements[$sid]['answers'][$key] = [
						'ACTIVE'  => $answer['ACTIVE'],
						'ID'      => $answer['ID'],
						'MESSAGE' => $answer['MESSAGE'],
						'PARAM'   => $answer['FIELD_PARAM'],
						'SORT'    => $answer['C_SORT'],
						'TYPE'    => $answer['FIELD_TYPE'],
					];
				}
			}

			return $elements;
		}

		/**
		 * Метод для получения имени элемента.
		 *
		 * @param array $args Массив аргументов.
		 *
		 * @return bool|null|string
		 */
		private function getElementName($args)
		{
			$name = null;

			if(!$args || !is_array($args) || !$args['ID'] || !$args['SID'] || !$args['TYPE'])
			{
				return false;
			}

			switch($args['TYPE'])
			{
				case 'text':
				case 'email':
				case 'file':
				case 'image':
				case 'url':
				case 'password':
				case 'date':
				case 'textarea':
					$name = "form_{$args['TYPE']}_{$args['ID']}";
					break;
				case 'radio':
				case 'checkbox':
				case 'dropdown':
				case 'multiselect':
					$name = "form_{$args['TYPE']}_{$args['SID']}";
					break;
			}

			if($args['TYPE'] === 'checkbox' || $args['TYPE'] === 'multiselect')
			{
				$name .= '[]';
			}

			return $name;
		}

		/**
		 * Метод для заполнения bitrix-формы данными.
		 *
		 * @param string|int $formId   Идентификатор формы.
		 * @param array      $formData Данные формы.
		 *
		 * @return bool|int
		 */
		private function addFormData($formId, $formData)
		{
			$result = false;

			if(!empty($formId) && !empty($formData) && is_array($formData))
			{
				$formResult = new \CFormResult();
				$result     = $formResult->Add($formId, $formData);

				$formResult->SetEvent($result);
				$formResult->Mail($result);

				\CFormCRM::onResultAdded($this->arParams['WEB_FORM_ID'], $result);
			}

			return $result;
		}

		/**
		 * Метод для подготовки данных для bitrix-формы.
		 *
		 * @param array $formData Данные формы.
		 *
		 * @return array
		 */
		private function prepareFormData($formData)
		{
			if($formData && is_array($formData))
			{
				if(isset($formData['CAPTCHA_KEY']))
				{
					unset($formData['CAPTCHA_KEY']);
				}

				if(isset($formData['CAPTCHA_SID']))
				{
					unset($formData['CAPTCHA_SID']);
				}
			}

			return $formData;
		}

		/**
		 * Метод для отправки электронного письма на указанные E-mail адреса.
		 *
		 * @param string         $eventMailTypeId Идентификатор типа почтового события.
		 * @param string|integer $mailTemplateId  Идентификатор почтового шаблона.
		 * @param array          $arEmails        Массив адресов электронной почты.
		 * @param array          $arFiles         Массив файлов.
		 */
		private function sendMessageToUsers($eventMailTypeId, $mailTemplateId, $arEmails, $arFiles = [])
		{
			if($eventMailTypeId && $mailTemplateId && !empty($arEmails) && is_array($arEmails))
			{
				foreach($arEmails as $email)
				{
					$this->sendMessage($eventMailTypeId, [SITE_ID], ['EMAIL' => $email], 'N', $mailTemplateId, $arFiles);
				}
			}
		}

		/**
		 * Метод для отправки писем.
		 *
		 * @param string         $eventMailTypeId Идентификатор типа почтового события.
		 * @param array          $siteIds         Массив идентификаторов сайта.
		 * @param array          $arFields        Массив полей типа почтового события.
		 * @param string         $duplicate       Значение отправки копии письма.
		 * @param string|integer $mailTemplateId  Идентификатор почтового шаблона.
		 * @param array          $arFiles         Массив файлов.
		 * @param string         $languageId      Идентификатор языковой версии.
		 *
		 * @return array|bool|int
		 */
		private function sendMessage($eventMailTypeId = null, $siteIds = [SITE_ID], $arFields = [], $duplicate = 'N', $mailTemplateId = null, $arFiles = [], $languageId = null)
		{
			return \CEvent::Send($eventMailTypeId, $siteIds, $arFields, $duplicate, $mailTemplateId, $arFiles, $languageId);
		}

		/**
		 * Метод для получения E-mail адресов с клиентской формы.
		 *
		 * @param array $formData Данные формы.
		 *
		 * @return array
		 */
		private function getEmails($formData)
		{
			$data     = $this->getFormDataByID($this->arParams['WEB_FORM_ID']);
			$items    = $this->getFormElements($data['questions'], $data['answers']);
			$arEmails = [];

			unset($data);

			foreach($items as $question)
			{
				$name = str_replace('[]', '', $question['NAME']);

				if($question['TYPE'] === 'email' && !empty($formData[$name]))
				{
					$arEmails[] = $formData[$name];
				}
			}

			unset($items);

			return $arEmails;
		}

		/**
		 * Метод для получения файлов с клиентской формы.
		 *
		 * @param boolean|integer $formDataResult Данные формы.
		 *
		 * @return array
		 */
		private function getFiles($formDataResult)
		{
			$arData = [];

			if($formDataResult && $this->arParams['WEB_FORM_ID'])
			{
				$formResult = new \CFormResult();
				$formResult->GetDataByID($formDataResult, [], $arResult, $arAnswer);

				foreach($arAnswer as $answer)
				{
					if(is_array($answer))
					{
						foreach($answer as $data)
						{
							if(is_array($data) && ($data['FIELD_TYPE'] === 'image' || $data['FIELD_TYPE'] === 'file') && $data['USER_FILE_ID'])
							{
								$arData[] = $data['USER_FILE_ID'];
							}
						}
					}
				}
			}

			return $arData;
		}

		/**
		 * Метод для получения параметров формы по её идентификатору.
		 *
		 * @param {integer|string} $formId Идентификатор формы.
		 *
		 * @return array|bool
		 */
		private function getFormParametersByID($formId)
		{
			if(!$formId)
			{
				return false;
			}

			return \CForm::GetByID($formId)
						 ->Fetch()
				;
		}

		/**
		 * Метод для генерации captcha.
		 *
		 * @return string
		 */
		private function generateCaptcha()
		{
			$captcha    = new \CCaptcha();
			$captchaKey = randString(10);

			$captcha->SetCodeCrypt($captchaKey);

			return htmlspecialchars($captcha->GetCodeCrypt());
		}

		/**
		 * Метод для получения нового значения captcha.
		 *
		 * @return array
		 */
		public function refreshCaptchaAction()
		{
			return [
				'captcha' => $this->generateCaptcha()
			];
		}

		/**
		 * Метод для описания pre- и post- фильтров ajax-запросов.
		 *
		 * @return array
		 */
		public function configureActions()
		{
			return [
				'refreshCaptcha' => [
					'prefilters'  => [],
					'postfilters' => []
				],
				'formData'       => [
					'prefilters'  => [],
					'postfilters' => []
				],
			];
		}
	}
