<?

	if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	{
		die();
	}

	use Bitrix\Main\Localization\Loc;

	$arComponentDescription = [
		'NAME'         => Loc::getMessage('WEB_FORM_AJAX_COMPONENT_NAME'),
		'DESCRIPTION'  => Loc::getMessage('WEB_FORM_AJAX_DESCRIPTION_COMPONENT'),
		'ICON'         => '',
		'PATH'         => [
			'ID'    => 'local',
			'NAME'  => Loc::getMessage('WEB_FORM_AJAX_COMPONENT_PATH_NAME'),
			'CHILD' => [
				'ID'   => 'webforms',
				'NAME' => Loc::getMessage('WEB_FORM_AJAX_COMPONENT_CHILD_PATH_NAME'),
			],
		],
		'AREA_BUTTONS' => [],
		'CACHE_PATH'   => 'Y',
		'COMPLEX'      => 'N'
	];
