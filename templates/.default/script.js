(function() {
	'use strict';

	// Полифилл для closest.

	// Проверяем поддержку метода closest.
	if(!Element.prototype.closest) {

		// Если метод closest не поддерживается - реализуем его.
		Element.prototype.closest = function(css) {
			var node = this;

			while(node) {
				if(node.matches(css)) {
					return node;
				}
				else {
					node = node.parentElement;
				}
			}

			return null;
		};
	}
})();

(function(_window, _document) {
	'use strict';

	/**
	 *
	 * @constructor
	 */
	function Element() {}

	/**
	 * Метод для получения значений с DOM-элементов.
	 *
	 * @param element DOM-элемент.
	 *
	 * @returns {Array}
	 */
	Element.prototype.values = function(element) {
		var values = [];

		if(element && typeof element.nodeName !== 'undefined') {
			switch(element.nodeName) {
				case 'INPUT':
					values = getValuesInput(element);
					break;
				case 'SELECT':
					values = getValuesSelect(element);
					break;
				case 'TEXTAREA':
					values = getValueTextArea(element);
					break;
			}
		}

		if(!Object.keys(values).length) {
			values = null;
		}

		return values;
	};

	/**
	 * Метод для получения значений с INPUT DOM-элемента.
	 *
	 * @param inputElement INPUT DOM-элемент.
	 *
	 * @returns {Array}
	 */
	function getValuesInput(inputElement) {
		var values = [];

		if(inputElement && typeof inputElement.nodeName !== 'undefined' && inputElement.nodeName === 'INPUT') {
			switch(inputElement.type) {
				case 'file':
					if(inputElement.files.length) {
						values = inputElement.files;
					}
					break;
				case 'checkbox':
				case 'radio':
					if(inputElement.checked && inputElement.value) {
						values.push(inputElement.value);
					}
					break;
				default:
					if(inputElement.value) {
						values.push(inputElement.value);
					}
			}
		}

		return values;
	}

	/**
	 * Метод для получения значений с SELECT DOM-элемента.
	 *
	 * @param selectElement SELECT DOM-элемент.
	 *
	 * @returns {Array}
	 */
	function getValuesSelect(selectElement) {
		var optionsElement;
		var countOptionsElement;
		var optionElement;
		var values = [];

		if(selectElement && typeof selectElement.nodeName !== 'undefined' && selectElement.nodeName === 'SELECT') {
			optionsElement      = selectElement.options;
			countOptionsElement = selectElement.options.length;

			for(var optionItem = 0; optionItem < countOptionsElement; optionItem++) {
				optionElement = optionsElement[optionItem];

				if(optionElement.selected && optionElement.value) {
					values.push(optionElement.value);
				}
			}
		}

		return values;
	}

	/**
	 * Метод для получения значения с TEXTAREA DOM-элемента.
	 *
	 * @param textAreaElement TEXTAREA DOM-элемент.
	 *
	 * @returns {Array}
	 */
	function getValueTextArea(textAreaElement) {
		var values = [];

		if(textAreaElement && typeof textAreaElement.nodeName !== 'undefined' && textAreaElement.nodeName === 'TEXTAREA') {
			if(textAreaElement.value) {
				values.push(textAreaElement.value);
			}
		}

		return values;
	}

	/**
	 * Метод для добавления какого-либо события на документ.
	 *
	 * @param {string} eventName Название события.
	 * @param {string} selector Селектор target-элемента.
	 * @param {function} callbackFunction Функция обратного вызова, возвращающая объект события и target-элемент.
	 *
	 * @returns {boolean}
	 */
	Element.prototype.addEventToDocument = function(eventName, selector, callbackFunction) {
		if(!eventName || typeof eventName !== 'string' || !selector || typeof selector !== 'string' || !callbackFunction || typeof callbackFunction !== 'function') {
			return false;
		}

		var elements;
		var elementsLen;
		var currentElement;

		if(typeof _document.querySelectorAll === 'function' && typeof _document.addEventListener === 'function') {
			elements    = _document.querySelectorAll(selector);
			elementsLen = (elements && typeof elements.length !== 'undefined') ? elements.length : 0;

			if(elements) {
				_document.addEventListener(eventName, function(e) {
					e = e || _window.event;

					currentElement = e.target || e.srcElement;

					for(var i = 0; i < elementsLen; i++) {
						if(currentElement && currentElement === elements[i]) {
							if(callbackFunction(e, currentElement) === false) {
								e.preventDefault();
							}

							break;
						}
					}
				});
			}
		}
	};

	if(!_window.webFormAjax) {
		_window.webFormAjax = {};
	}

	if(typeof _window.webFormAjax === 'object') {
		_window.webFormAjax.Element = new Element;
	}
})(window, document);

(function(_window) {
	'use strict';

	/**
	 *
	 * @constructor
	 */
	function AjaxComponent() {
		var self = this;

		self.mode      = 'ajax';
		self.component = 'bitrix:webform.ajax';
	}

	/**
	 * Метод для вызова методов из какого-либо компонента.
	 *
	 * @param {string} component Полное название компонента.
	 * @param {string} action Название вызываемого метода из компонента.
	 * @param {object} data Данные, передаваемые в вызываемый метод компонента.
	 * @param {function} callbackFunction Функция обратного вызова, возвращающая результат с вызываемого метода компонента.
	 *
	 * @returns {boolean}
	 */
	AjaxComponent.prototype.runAction = function(component, action, data, callbackFunction) {
		if(typeof BX === 'undefined' || !component || !action || typeof data !== 'object' || typeof callbackFunction !== 'function') {
			return false;
		}

		var self = this;

		BX.ajax.runComponentAction(component, action, {
			mode : self.mode,
			data : data
		})
		  .then(function(response) {
			  if(response && typeof response === 'object' && response['status'] === 'success' && typeof response['data'] === 'object') {
				  callbackFunction(response['data']);
			  }
		  });
	};

	if(!_window.webFormAjax) {
		_window.webFormAjax = {};
	}

	if(typeof _window.webFormAjax === 'object') {
		_window.webFormAjax.AjaxComponent = new AjaxComponent;
	}
})(window);

(function(_window) {
	'use strict';

	/**
	 *
	 * @constructor
	 */
	function ComponentParameters() {
		var self = this;

		self.showErrors = false;
	}

	/**
	 * Метод для установки значения параметра "показывать ошибки".
	 *
	 * @param {string} value Значение.
	 *
	 * @returns {boolean}
	 */
	ComponentParameters.prototype.setShowErrors = function(value) {
		if(!value) {
			return false;
		}

		var self = this;

		if(value === 'Y') {
			self.showErrors = true;
		}
		else {
			if(value === 'N') {
				self.showErrors = false;
			}
		}
	};

	/**
	 * Метод для получения значения параметра "показывать ошибки".
	 */
	ComponentParameters.prototype.getShowErrors = function() {
		var self = this;
		var value;

		if(self.showErrors === true) {
			value = 'Y';
		}
		else {
			if(self.showErrors === false) {
				value = 'N';
			}
		}

		return value;
	};

	if(!_window.webFormAjax) {
		_window.webFormAjax = {};
	}

	if(typeof _window.webFormAjax === 'object') {
		_window.webFormAjax.ComponentParameters = new ComponentParameters;
	}
})(window);

(function(_window, _document) {
	'use strict';

	if(typeof webFormAjax !== 'object' || typeof webFormAjax.Element !== 'object' || typeof webFormAjax.AjaxComponent !== 'object' || typeof webFormAjax.ComponentParameters !== 'object') {
		return false;
	}

	/**
	 *
	 * @constructor
	 */
	function Captcha() {
		var self = this;

		self.captchaUrl = '/bitrix/tools/captcha.php';
	}

	/**
	 * Метод инициализации конструктора Captcha.
	 *
	 * @constructor
	 */
	Captcha.prototype.Init = function() {
		var self = this;
		var captchaHiddenElement;

		if(typeof _document.querySelector === 'function') {
			self.captchaHiddenElement = _document.querySelector('[data-webform-ajax-captcha-hidden]');
			self.captchaKeyElement    = _document.querySelector('[data-webform-ajax-captcha-key-element]');
			self.captchaImgElement    = _document.querySelector('[data-webform-ajax-captcha-img]');
		}

		webFormAjax.Element.addEventToDocument('click', '[data-webform-ajax-captcha-img]', function(e, currentElement) {
			self.get(function(captcha) {
				self.set({
					'captcha'              : captcha,
					'captchaImgElement'    : currentElement,
					'captchaHiddenElement' : self.captchaHiddenElement
				});

				if(typeof self.captchaKeyElement.value !== 'undefined') {
					self.captchaKeyElement.value = null;
				}
			});
		});

		if(typeof BX !== 'undefined') {
			self.webFormCaptchaError = BX.message('WEB_FORM_CAPTCHA_ERROR');
		}
	};

	/**
	 * Метод для получения значения captcha.
	 *
	 * @param {function} callbackFunction Функция обратного вызова, возвращающая captcha.
	 */
	Captcha.prototype.get = function(callbackFunction) {
		webFormAjax.AjaxComponent.mode = 'class';
		webFormAjax.AjaxComponent.runAction(webFormAjax.AjaxComponent.component, 'refreshCaptcha', {}, function(data) {
			if(data && typeof data === 'object' && data['captcha'] && callbackFunction && typeof callbackFunction === 'function') {
				callbackFunction(data['captcha']);
			}
		});
	};

	/**
	 * Метод для установки captcha.
	 *
	 * @param {object} args Параметры.
	 */
	Captcha.prototype.set = function(args) {
		var self = this;

		if(typeof args === 'object' && args.captcha && args.captchaImgElement && args.captchaHiddenElement) {
			if(typeof args.captchaImgElement.src !== 'undefined' && typeof args.captchaHiddenElement.value !== 'undefined') {
				args.captchaImgElement.src      = self.captchaUrl + '?captcha_code=' + args.captcha;
				args.captchaHiddenElement.value = args.captcha;
			}
		}
	};

	/**
	 * Метод обработки captcha.
	 *
	 * @param {string} response Ответ сервера.
	 */
	Captcha.prototype.captchaProcessing = function(response) {
		var self = this;

		if(response === 'CAPTCHA_ERROR') {
			if(typeof BX !== 'undefined') {
				BX.addClass(self.captchaKeyElement, 'webform-ajax__element-error');
			}

			if(webFormAjax.ComponentParameters.showErrors) {
				webFormAjax.Form.showTextError(self.captchaKeyElement, self.webFormCaptchaError);
			}

			self.get(function(captcha) {
				self.set({
					'captcha'              : captcha,
					'captchaImgElement'    : self.captchaImgElement,
					'captchaHiddenElement' : self.captchaHiddenElement
				});
			});

			if(typeof self.captchaKeyElement.value !== 'undefined') {
				self.captchaKeyElement.value = null;
			}
		}
		else {
			if(response === 'CAPTCHA_SUCCESS') {
				if(typeof BX !== 'undefined') {
					BX.removeClass(self.captchaKeyElement, 'webform-ajax__element-error');
				}

				if(webFormAjax.ComponentParameters.showErrors) {
					webFormAjax.Form.hideTextError(self.captchaKeyElement, self.webFormCaptchaError);
				}
			}
		}
	};

	if(!_window.webFormAjax) {
		_window.webFormAjax = {};
	}

	if(typeof _window.webFormAjax === 'object') {
		_window.webFormAjax.Captcha = new Captcha;
	}
})(window, document);

(function(_window, _document) {
	'use strict';

	if(typeof webFormAjax !== 'object' || typeof webFormAjax.Element !== 'object' || typeof webFormAjax.Captcha !== 'object' || typeof webFormAjax.ComponentParameters !== 'object'/* || typeof webFormAjax.AjaxComponent !== 'object' */) {
		return false;
	}

	/**
	 *
	 * @constructor
	 */
	function Form() {}

	/**
	 * Метод инициализации конструктора Form.
	 *
	 * @constructor
	 */
	Form.prototype.Init = function() {
		var self = this;
		var formDataElements;
		var formData;

		webFormAjax.Element.addEventToDocument('submit', '[data-webform-ajax-form-submit]', function(e, currentElement) {
			formDataElements = self.getDataItems(currentElement);

			if(self.checkFormData(formDataElements)) {
				// formData = self.prepareFormData(formDataElements);
				formData = new FormData(currentElement);
				formData.append('FORM_DATA', 'Y');

				// webFormAjax.AjaxComponent.mode = 'class';
				// webFormAjax.AjaxComponent.runAction(webFormAjax.AjaxComponent.component, 'formData', {'formData' : JSON.stringify(formData)}, function(data) {
				// 	if(data && typeof data === 'object' && data['response']) {}
				// });

				if(_window.$ && typeof $ !== 'object' && typeof $.ajax === 'function') {
					// Отправка на сервер
					$.ajax({
						url         : location.pathname,
						data        : formData,
						dataType    : 'json',
						type        : 'POST',
						timeout     : 15000,
						cache       : false,
						contentType : false,
						processData : false,
						beforeSend  : function() {
							if(typeof BX !== 'undefined' && self.ajaxLoaderElement) {
								BX.removeClass(self.ajaxLoaderElement, 'webform-ajax__hidden');
							}
						},
						complete    : function() {
							if(typeof BX !== 'undefined' && self.ajaxLoaderElement) {
								BX.addClass(self.ajaxLoaderElement, 'webform-ajax__hidden');
							}
						},
						error       : function(jqXHR, textStatus) {
							console.error(textStatus);
						}
					})
					 .done(function(data) {
						 console.log(data);

						 self.hideTextBlock(self.messageBlock, self.messageBlockTextSuccess, self.textSuccess);
						 self.hideTextBlock(self.messageBlock, self.messageBlockTextError, self.textError);
						 webFormAjax.Captcha.captchaProcessing('CAPTCHA_SUCCESS');

						 if(data && typeof data === 'object' && data['response']) {
							 if(data['response']['success'] && typeof data['response']['success'] === 'object') {
								 if(data['response']['success']['status_code'] === 'Y') {
									 self.showTextBlock(self.messageBlock, self.messageBlockTextSuccess, self.textSuccess, self.messageSendDataSuccess);
								 }
							 }

							 if(data['response']['error'] && typeof data['response']['error'] === 'object') {
								 if(data['response']['error']['error_code'] === 'captcha_error') {
									 webFormAjax.Captcha.captchaProcessing('CAPTCHA_ERROR');
								 }
								 else {
									 self.showTextBlock(self.messageBlock, self.messageBlockTextError, self.textError, self.messageSendDataError);
								 }
							 }
						 }
					 });
				}
			}
			else {
				scrollTo(0, 0);
			}

			return false;
		});

		webFormAjax.Element.addEventToDocument('click', '[data-webform-ajax-close-button-message-block]', function() {
			self.hideTextBlock(self.messageBlock, self.messageBlockTextSuccess, self.textSuccess);
			self.hideTextBlock(self.messageBlock, self.messageBlockTextError, self.textError);
		});

		if(typeof BX !== 'undefined') {
			self.messageValueFieldNotFound = BX.message('WEB_FORM_VALUE_FIELD_NOT_FOUND');
			self.messageSendDataSuccess    = BX.message('WEB_FORM_SEND_DATA_SUCCESS');
			self.messageSendDataError      = BX.message('WEB_FORM_SEND_DATA_ERROR');
		}

		self.ajaxLoaderElement       = _document.querySelector('[data-webform-ajax-loader]');
		self.messageBlock            = _document.querySelector('[data-webform-ajax-message-block]');
		self.messageBlockTextSuccess = _document.querySelector('[data-webform-ajax-message-block-text-success]');
		self.textSuccess             = _document.querySelector('[data-webform-ajax-message-block-text-success] > [data-webform-ajax-text-success]');
		self.messageBlockTextError   = _document.querySelector('[data-webform-ajax-message-block-text-error]');
		self.textError               = _document.querySelector('[data-webform-ajax-message-block-text-error] > [data-webform-ajax-text-error]');
	};

	/**
	 * Метод для получения данных с элементов формы.
	 *
	 * @param formElement Элемент формы.
	 *
	 * @returns {object}
	 */
	Form.prototype.getDataItems = function(formElement) {
		var formElements;
		var formElementsLen;
		var element;
		var isArrElementName = false;
		var elementName;
		var data             = {};
		var values;
		var valuesLen        = 0;

		if(formElement) {
			formElements    = formElement.elements;
			formElementsLen = formElements.length;

			for(var formItem = 0; formItem < formElementsLen; formItem++) {
				element          = formElements[formItem];
				isArrElementName = (element.name.indexOf('[]') !== -1);
				elementName      = (isArrElementName) ? element.name.replace('[]', '') : element.name;

				if(elementName && !data[elementName]) {
					data[elementName] = {
						'data'       : [],
						'element'    : element,
						'properties' : {
							'required'            : element.hasAttribute('data-webform-ajax-required'),
							'is_arr_element_name' : isArrElementName
						}
					};
				}

				if(typeof data[elementName] !== 'undefined' && typeof data[elementName]['data'] !== 'undefined') {
					values = webFormAjax.Element.values(element);

					if(values && typeof values === 'object') {
						valuesLen = values.length;

						if(valuesLen && valuesLen === 1 && !(values instanceof FileList)) {
							data[elementName]['data'].push(values[valuesLen - 1]);
						}
						else {
							if(valuesLen) {
								data[elementName]['data'] = values;
							}
						}
					}
				}
			}
		}

		return data;
	};

	/**
	 * Метод для проверки полей формы.
	 *
	 * @param formData Данные полей формы.
	 *
	 * @returns {boolean}
	 */
	Form.prototype.checkFormData = function(formData) {
		var self = this;
		var data = {};
		var flag = true;

		if(formData && typeof formData === 'object') {
			for(var key in formData) {
				data = formData[key];

				if(typeof data === 'object' && typeof data.properties === 'object' && data.properties.required && typeof data['data'] === 'object') {
					if(Object.keys(data['data']).length) {
						if(typeof BX !== 'undefined') {
							BX.removeClass(data.element, 'webform-ajax__element-error');
						}

						if(webFormAjax.ComponentParameters.showErrors) {
							self.hideTextError(data.element);
						}
					}
					else {
						flag = false;

						if(typeof BX !== 'undefined') {
							BX.addClass(data.element, 'webform-ajax__element-error');
						}

						if(webFormAjax.ComponentParameters.showErrors) {
							self.showTextError(data.element, self.messageValueFieldNotFound);
						}
					}
				}
			}
		}

		return flag;
	};

	/**
	 * Метод для скрытия ошибки.
	 *
	 * @param element Элемент.
	 */
	Form.prototype.hideTextError = function(element) {
		var parentElement;
		var textErrorElement;

		if(element) {
			parentElement = element.closest('[data-webform-ajax-form-element]');

			if(parentElement && typeof parentElement.querySelector === 'function') {
				textErrorElement = parentElement.querySelector('[data-webform-ajax-text-error]');

				if(textErrorElement.innerText) {
					textErrorElement.innerText = null;
				}

				if(typeof BX !== 'undefined') {
					BX.addClass(textErrorElement, 'webform-ajax__hidden');
				}
			}
		}
	};

	/**
	 * Метод для показа ошибки.
	 *
	 * @param element Элемент.
	 *
	 * @param {string} message Сообщение об ошибке.
	 */
	Form.prototype.showTextError = function(element, message) {
		var parentElement;
		var textErrorElement;

		if(element && message) {
			parentElement = element.closest('[data-webform-ajax-form-element]');

			if(parentElement && typeof parentElement.querySelector === 'function') {
				textErrorElement = parentElement.querySelector('[data-webform-ajax-text-error]');

				if(textErrorElement.innerText !== message) {
					textErrorElement.innerText = message;
				}

				if(typeof BX !== 'undefined') {
					BX.removeClass(textErrorElement, 'webform-ajax__hidden');
				}
			}
		}
	};

	/**
	 * Метод для скрытия текста в блоке.
	 *
	 * @param messageBlockElement Элемент блока.
	 * @param textBlockElement Элемент текстового блока.
	 * @param textElement Текстовый элемент.
	 */
	Form.prototype.hideTextBlock = function(messageBlockElement, textBlockElement, textElement) {
		if(messageBlockElement && textBlockElement && textElement) {
			if(typeof BX !== 'undefined') {
				BX.addClass(messageBlockElement, 'webform-ajax__hidden');
				BX.addClass(textBlockElement, 'webform-ajax__hidden');

				if(textElement.innerText) {
					textElement.innerText = null;
				}
			}
		}
	};

	/**
	 * Метод для отображения текста в блоке.
	 *
	 * @param messageBlockElement Элемент блока.
	 * @param textBlockElement Элемент текстового блока.
	 * @param textElement Текстовый элемент.
	 * @param {string} message Текст сообщения.
	 */
	Form.prototype.showTextBlock = function(messageBlockElement, textBlockElement, textElement, message) {
		if(messageBlockElement && textBlockElement && textElement && message) {
			if(typeof BX !== 'undefined') {
				if(textElement.innerText !== message) {
					textElement.innerText = message;
				}

				BX.removeClass(textBlockElement, 'webform-ajax__hidden');
				BX.removeClass(messageBlockElement, 'webform-ajax__hidden');
			}
		}
	};

	/**
	 * Метод подготовки данных формы для отправки.
	 *
	 * @param {object} data
	 *
	 * @returns {*}
	 */
	Form.prototype.prepareFormData = function(data) {
		var formData = {};
		var arData;

		if(data && typeof data === 'object') {
			for(var name in data) {
				arData         = data[name];
				formData[name] = '';

				if(typeof arData === 'object' && arData['data'] && arData['properties']) {
					if(arData['data'].length === 1 && !arData['properties']['is_arr_element_name']) {
						formData[name] = arData['data'][0];
					}
					else {
						if(arData['data'].length >= 1) {
							formData[name] = arData['data'];
						}
					}
				}
			}
		}

		return formData;
	};

	if(!_window.webFormAjax) {
		_window.webFormAjax = {};
	}

	if(typeof _window.webFormAjax === 'object') {
		_window.webFormAjax.Form = new Form;
	}
})(window, document);
