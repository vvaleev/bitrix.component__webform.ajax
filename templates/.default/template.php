<?

	if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	{
		die();
	}

	use Bitrix\Main\Localization\Loc;

	$templatePath = $component->__template->GetFolder();

	$APPLICATION->AddHeadScript('/bitrix/js/main/jquery/jquery-1.8.3.js');

?>

<div class="webform-ajax">
	<? if($arResult['FORM_DATA']['QUESTIONS_COUNT'] > 1): ?>
		<form action="<?=$APPLICATION->GetCurPage()?>" class="webform-ajax__webform" method="post" data-webform-ajax-form-submit="webform-ajax">
			<div class="webform-ajax__form-name">
				<?=$arResult['FORM_DATA']['NAME']?>
			</div>

			<? foreach($arResult['FORM_DATA']['ITEMS'] as $arItem)
			{
				if($arItem['ACTIVE'] === 'Y')
				{
					?>
					<div class="webform-ajax__form-element" data-webform-ajax-form-element>
						<label class="webform-ajax__form-label-element">
							<? if($arItem['TITLE']): ?>
								<span class="webform-ajax__form-title-element">
									<?=$arItem['TITLE']?>:
								</span>
							<? endif; ?>

							<? if($arItem['TYPE'] === 'text' || $arItem['TYPE'] === 'email' || $arItem['TYPE'] === 'file' || $arItem['TYPE'] === 'image' || $arItem['TYPE'] === 'url' || $arItem['TYPE'] === 'password'): ?>
								<input class="webform-ajax__form-input-element" type="<? if($arItem['TYPE'] === 'image'): ?>file<? else: ?><?=$arItem['TYPE']?><? endif; ?>" name="<?=$arItem['NAME']?>" <? if($arItem['REQUIRED'] === 'Y'): ?>data-webform-ajax-required<? endif ?> />
							<? endif; ?>

							<? if($arItem['TYPE'] === 'date'): ?>
								<? if($arItem['REQUIRED'] === 'Y')
								{
									$requiredParam = 'data-webform-ajax-required';
								}
								?>
								<?
								$APPLICATION->IncludeComponent('bitrix:main.calendar', '', Array(
									'SHOW_INPUT'            => 'Y',
									'INPUT_NAME'            => "form_{$arItem['TYPE']}_{$arItem['answers'][0]['ID']}",
									'SHOW_TIME'             => 'Y',
									'HIDE_TIMEBAR'          => 'Y',
									'INPUT_ADDITIONAL_ATTR' => "class=\"webform-ajax__form-input-element\" placeholder=\"" . Loc::getMessage('WEB_FORM_CALENDAR_PLACEHOLDER') . "\" $requiredParam"
								));
								?>
							<? endif; ?>

							<? if($arItem['TYPE'] === 'radio' || $arItem['TYPE'] === 'checkbox'): ?>
								<div class="webform-ajax__group-elements">
									<? foreach($arItem['answers'] as $answer)
									{
										if($answer['ACTIVE'] === 'Y')
										{
											?>
											<label class="webform-ajax__form-sub-label-element">
												<input class="webform-ajax__form-input-element" value="<?=$answer['ID']?>" type="<?=$answer['TYPE']?>" name="<?=$arItem['NAME']?>" <? if($arItem['REQUIRED'] === 'Y'): ?>data-webform-ajax-required<? endif ?> <? if($answer['PARAM'] === 'checked'): ?>checked<? endif ?> />
												<?=$answer['MESSAGE']?>
											</label>
											<?
										}
									} ?>
								</div>
							<? endif ?>

							<? if($arItem['TYPE'] === 'textarea'): ?>
								<textarea class="webform-ajax__form-textarea-element" name="<?=$arItem['NAME']?>" <? if($arItem['REQUIRED'] === 'Y'): ?>data-webform-ajax-required<? endif ?>></textarea>
							<? endif; ?>

							<? if($arItem['TYPE'] === 'dropdown' || $arItem['TYPE'] === 'multiselect'): ?>
								<select class="webform-ajax__form-select-element" name="<?=$arItem['NAME']?>" <? if($arItem['REQUIRED'] === 'Y'): ?>data-webform-ajax-required<? endif ?> <? if($arItem['TYPE'] === 'multiselect'): ?>multiple<? endif ?>>
									<?
										foreach($arItem['answers'] as $answer)
										{
											if($answer['ACTIVE'] === 'Y')
											{
												?>
												<option class="webform-ajax__form-option-element" value="<?=$answer['ID']?>" <? if($answer['PARAM'] === 'selected'): ?>selected<? endif ?>>
													<?=$answer['MESSAGE']?>
												</option>
												<?
											}
										};
									?>
								</select>
							<? endif; ?>

							<? if($arItem['IMAGE_SRC']): ?>
								<img src="<?=$arItem['IMAGE_SRC']?>" alt="" class="webform-ajax__form-image-element" <?=$arItem['IMAGE_SIZE'][3]?> />
							<? endif; ?>
						</label>

						<div class="webform-ajax__text-error webform-ajax__hidden" data-webform-ajax-text-error></div>
					</div>
					<?
				}
			}
			?>

			<div class="webform-ajax__form-control-elements">
				<? if($arResult['FORM_DATA']['USE_CAPTCHA'] === 'Y'): ?>
				<div class="webform-ajax__captcha" data-webform-ajax-form-element>
					<label class="webform-ajax__form-label-element">
						<?=Loc::getMessage('WEB_FORM_CAPTCHA_TITLE')?>:
						<img src="/bitrix/tools/captcha.php?captcha_code=<?=$arResult['FORM_DATA']['CAPTCHA']?>" alt="" class="webform-ajax__captcha-image" data-webform-ajax-captcha-img />
						<input class="webform-ajax__form-input-element webform-ajax__captcha-key-element" type="text" name="CAPTCHA_KEY" data-webform-ajax-captcha-key-element />
						<input type="hidden" name="CAPTCHA_SID" value="<?=$arResult['FORM_DATA']['CAPTCHA']?>" data-webform-ajax-captcha-hidden />
					</label>
					<div class="webform-ajax__captcha-error-text webform-ajax__hidden" data-webform-ajax-text-error></div>
				</div>
				<? endif; ?>

				<button class="webform-ajax__form-submit-button-element">
					<span class="webform-ajax__button-submit-title">
						<?=$arResult['FORM_DATA']['SUBMIT_BUTTON_NAME']?>
					</span>
					<span class="webform-ajax__button-submit-image webform-ajax__hidden" data-webform-ajax-loader>
						<img src="<?=$templatePath?>/image/loading.gif" alt="" />
					</span>
				</button>
			</div>
		</form>
	<? else: ?>
		<div class="webform-ajax__message-danger">
			<?=Loc::getMessage('MESSAGE_FORM_ELEMENTS_NOT_FOUND')?>
		</div>
	<? endif ?>

	<div class="webform-ajax__message-block webform-ajax__hidden" data-webform-ajax-message-block>
		<div class="webform-ajax__message-block-text-success webform-ajax__hidden" data-webform-ajax-message-block-text-success>
			<span data-webform-ajax-text-success></span>
			<div class="webform-ajax__close-button-message-block" data-webform-ajax-close-button-message-block>X</div>
		</div>
		<div class="webform-ajax__message-block-text-error webform-ajax__hidden" data-webform-ajax-message-block-text-error>
			<span data-webform-ajax-text-error></span>
			<div class="webform-ajax__close-button-message-block" data-webform-ajax-close-button-message-block>X</div>
		</div>
	</div>
</div>

<script>
	if(typeof BX !== 'undefined') {
		BX.ready(function() {
			BX.message({'WEB_FORM_VALUE_FIELD_NOT_FOUND' : '<?=Loc::getMessage('WEB_FORM_VALUE_FIELD_NOT_FOUND')?>'});
			BX.message({'WEB_FORM_CAPTCHA_ERROR' : '<?=Loc::getMessage('WEB_FORM_CAPTCHA_ERROR')?>'});
			BX.message({'WEB_FORM_SEND_DATA_SUCCESS' : '<?=Loc::getMessage('WEB_FORM_SEND_DATA_SUCCESS')?>'});
			BX.message({'WEB_FORM_SEND_DATA_ERROR' : '<?=Loc::getMessage('WEB_FORM_SEND_DATA_ERROR')?>'});

			if(typeof webFormAjax === 'object') {
				<?if($arParams['SHOW_ERRORS']):?>
				if(typeof webFormAjax.ComponentParameters === 'object' && webFormAjax.ComponentParameters.getShowErrors() !== '<?=$arParams['SHOW_ERRORS']?>') {
					webFormAjax.ComponentParameters.setShowErrors("<?=$arParams['SHOW_ERRORS']?>");
				}
				<?endif;?>

				if(typeof webFormAjax.AjaxComponent === 'object') {
					webFormAjax.AjaxComponent.component = '<?=$component->GetName()?>';
				}

				if(typeof webFormAjax.Captcha === 'object') {
					webFormAjax.Captcha.Init();
				}

				if(typeof webFormAjax.Form === 'object') {
					webFormAjax.Form.Init();
				}
			}
		});
	}
</script>
