<?

	if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	{
		die();
	}

	$MESS['MESSAGE_FORM_ELEMENTS_NOT_FOUND'] = 'Элементы формы не найдены';
	$MESS['WEB_FORM_CAPTCHA_TITLE']          = 'Введите капчу';
	$MESS['WEB_FORM_CAPTCHA_ERROR']          = 'Капча введена неверно';
	$MESS['WEB_FORM_VALUE_FIELD_NOT_FOUND']  = 'Поле не заполнено';
	$MESS['WEB_FORM_SEND_DATA_SUCCESS']      = 'Данные успешно отправлены';
	$MESS['WEB_FORM_SEND_DATA_ERROR']        = 'Не удалось отправить данные';
	$MESS['WEB_FORM_CALENDAR_PLACEHOLDER']   = 'дд.мм.гггг';
