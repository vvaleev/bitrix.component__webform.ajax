<?

	if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	{
		die();
	}

	$MESS['WEB_FORM_AJAX_COMPONENT_NAME']            = 'AJAX веб-форма';
	$MESS['WEB_FORM_AJAX_DESCRIPTION_COMPONENT']     = 'Компонент веб-форм';
	$MESS['WEB_FORM_AJAX_COMPONENT_PATH_NAME']       = 'Компоненты проекта';
	$MESS['WEB_FORM_AJAX_COMPONENT_CHILD_PATH_NAME'] = 'Веб-формы';
