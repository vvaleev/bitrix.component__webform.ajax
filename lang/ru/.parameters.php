<?

	if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	{
		die();
	}

	$MESS['WEB_FORM_AJAX_PARAMETER_SHOW_ERRORS']            = 'Показывать ошибки';
	$MESS['WEB_FORM_AJAX_PARAMETER_WEB_FORM_ID']            = 'Идентификатор веб-формы';
	$MESS['WEB_FORM_AJAX_PARAMETER_NOT_VALUE']              = '(не выбрано)';
	$MESS['WEB_FORM_AJAX_PARAMETER_EVENT_MAIL_TYPE_ID']     = 'Тип почтового события';
	$MESS['WEB_FORM_AJAX_PARAMETER_EVENT_MAIL_TEMPLATE_ID'] = 'Почтовый шаблон';
